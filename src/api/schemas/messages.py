from pydantic import BaseModel


class RequestMessage(BaseModel):
    message: str


class ResponseMessage(BaseModel):
    response_message: list[str]


class Message(BaseModel):
    id: str
    is_from_system: bool
    message: str

from .login import LoginRequestSchema

__all__ = (
    'LoginRequestSchema',
)

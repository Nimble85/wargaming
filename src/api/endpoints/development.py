import random
import faker
from fastapi import APIRouter, HTTPException, Header
from typing import Annotated

from src.api.schemas import (
    Game,
    Message,
    RequestMessage,
    ResponseMessage,
    State,
    Card,
    LoginRequestSchema,
    LoginResponseSchema,
    PingResponseSchema,
    User,

)

dev_router = APIRouter()

users = [
    User(user_id="777", login="admin", password="admin", is_admin=True, games=[]),
    User(user_id='1', login='player', password='player', is_admin=False, games=[
        Game(
            game_id='1',
            name='My_game',
            turn_number=10,
            messages=[
                Message(
                    id='123',
                    is_from_system=False,
                    message='Initiating game',
                ),
                Message(
                    id='124',
                    is_from_system=True,
                    message='Game initiated',
                ),
                Message(
                    id='125',
                    is_from_system=True,
                    message='Random message from the system',
                ),
            ],
            state=State(
                health=100,
                player_cards=[
                    Card(
                        title='IPSO',
                        description='This is mega IPSO',
                        image_url='http://image.com/image.png',
                        quantity=1,
                        is_used=False
                    ),
                    Card(
                        title='IPSO2',
                        description='This is mega IPSO2',
                        image_url='http://image.com/image.png',
                        quantity=1,
                        is_used=False
                    )
                ],
                allys_cards=[
                    Card(
                        title='antiIPSO',
                        description='This is anti IPSO',
                        image_url='http://image.com/image.png',
                        quantity=1,
                        is_used=False
                    )
                ],
                enemy_cards=[
                    Card(
                        title='Tanks',
                        description='This is Tanks',
                        image_url='http://image.com/image.png',
                        quantity=10,
                        is_used=False
                    )
                ],
                enemy_allys_cards=[
                    Card(
                        title='Aviation',
                        description='This is Aviation',
                        image_url='http://image.com/image.png',
                        quantity=10,
                        is_used=False
                    )
                ]
            )
        ),
    Game(
        game_id='2',
        name='My_game2',
        turn_number=0,
        messages=[],
        state=None
    )
    ])
]


@dev_router.get(path='/ping', response_model=PingResponseSchema)
def ping():
    return PingResponseSchema()


@dev_router.post(path='/login', response_model=LoginResponseSchema)
def login(login_body: LoginRequestSchema):
    for user in users:
        if user.login == login_body.login and user.password == login_body.password:
            return user.model_dump(include={'user_id', 'is_admin'})

    raise HTTPException(status_code=401, detail="Unauthorized")


@dev_router.get(path='/user/{user_id}/games')
def games_by_user_id(user_id: str, game_result: Annotated[str | None, Header(convert_underscores=False)] = None):
    if game_result:
        return {"Game_Result": game_result}
    for user in users:
        if user.user_id == user_id:
            return [game.model_dump(exclude={'turns'}) for game in user.games]
    raise HTTPException(status_code=404, detail="User not found")


@dev_router.get(path='/games/{game_id}', response_model=Game)
def games_by_id(game_id: str):
    for user in users:
        for game in user.games:
            if game_id == game.game_id:
                return game.model_dump()
    raise HTTPException(status_code=404, detail="Game not found")


@dev_router.post(path='/games/{game_id}/chat', response_model=ResponseMessage)
def send_message_to_chat(game_id: str, message: RequestMessage):
    for user in users:
        for game in user.games:
            if game.game_id == game_id:
                user_message = Message(
                    id=str(random.randint(125, 10000)),
                    is_from_system=False,
                    message=message.message,
                )
                game.messages.append(user_message)

                answer = faker.Faker().sentence()

                system_message = Message(
                    id=str(random.randint(125, 10000)),
                    is_from_system=True,
                    message=answer,
                )
                game.messages.append(system_message)
                return ResponseMessage(response_message=[answer]).model_dump()

    raise HTTPException(status_code=404, detail="Game not found")

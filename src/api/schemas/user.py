from pydantic import BaseModel

from .game import Game


class User(BaseModel):
    user_id: str
    login: str
    password: str
    is_admin: bool
    games: list[Game]

from pydantic import BaseModel

from .messages import Message


class Coords(BaseModel):
    x: float
    y: float


class Card(BaseModel):
    title: str
    description: str
    image_url: str
    quantity: int
    is_used: bool
    coords: Coords | None = None
    possible_locations: list[Coords] | None = None


class State(BaseModel):
    health: float
    player_cards: list[Card]
    allys_cards: list[Card]
    enemy_cards: list[Card]
    enemy_allys_cards: list[Card]


class Game(BaseModel):
    game_id: str
    name: str
    turn_number: int
    messages: list[Message] | None = []
    state: State | None = []

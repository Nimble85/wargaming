import { capitalize } from 'lodash';

export const capitalizeFirstLetterInSentence = (text: string): string =>
  text.split('. ').map(capitalize).join('. ');

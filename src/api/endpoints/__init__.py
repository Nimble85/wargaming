from fastapi import APIRouter

from .development import dev_router
from .production import prod_router

from src.core.config import get_settings

root_router = APIRouter()

environment = get_settings().ENVIRONMENT

if environment == 'dev':
    root_router.include_router(dev_router, tags=['Development'])
else:
    root_router.include_router(prod_router, tags=['Production'])

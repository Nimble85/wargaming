from fastapi import APIRouter

from src.api.schemas import (
    Game,
    LoginRequestSchema,
    LoginResponseSchema,
    PingResponseSchema,
)

prod_router = APIRouter()


@prod_router.get(path='/ping', response_model=PingResponseSchema)
def ping():
    return PingResponseSchema()


@prod_router.post(path='/login', response_model=LoginResponseSchema)
def login(login_body: LoginRequestSchema):
    ...


@prod_router.get(path='/user/{user_id}/games')
def games_by_user_id(user_id: str):
    ...


@prod_router.get(path='/games/{game_id}', response_model=Game)
def games_by_id(game_id: str):
    ...

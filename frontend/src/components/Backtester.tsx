import React from 'react';
import {
  Box,
  Button,
  Container,
  IconButton,
  OutlinedInput,
  Paper,
  Table,
  TableBody,
  TableCell,
  tableCellClasses,
  TableContainer,
  TableHead,
  TableRow,
  Tooltip,
  Typography,
} from '@mui/material';
import { range } from 'lodash';
import { FormProvider, useFieldArray, useForm } from 'react-hook-form';
import { AddCircleOutline } from '@mui/icons-material';
import { SymbolMatch } from '../types';
import { TickerShares } from './TickerShares';

const DEFAULT_PORTFOLIOS_NUMBER = 3;
const DEFAULT_TICKERS_NUMBER = 4;

export type Share = {
  share: string;
};

export type TickerRow = {
  ticker: SymbolMatch | null;
  shares: Share[];
};

export type Portfolio = {
  name: string;
  total: number;
};

export type FormValues = {
  tickers: TickerRow[];
  portfolios: Portfolio[];
};

export const Backtester: React.FC = () => {
  const defaultValues = {
    tickers: range(DEFAULT_TICKERS_NUMBER).map(() => ({
      ticker: null,
      shares: range(DEFAULT_PORTFOLIOS_NUMBER).map(() => ({
        share: '',
      })),
    })),
    portfolios: range(DEFAULT_PORTFOLIOS_NUMBER).map((num) => ({
      name: `Portfolio ${num + 1}`,
      total: 0,
    })),
  };

  const methods = useForm<FormValues>({
    defaultValues,
    shouldUnregister: true,
    criteriaMode: 'all',
    mode: 'onSubmit',
  });

  const {
    register,
    control,
    handleSubmit,
    setValue,
    getValues,
    watch,
    getFieldState,
    clearErrors,
    formState,
  } = methods;

  // console.log(errors);

  const watchPortfolios = watch('portfolios');

  const { fields: rows, append } = useFieldArray<FormValues>({
    name: 'tickers',
    control,
    shouldUnregister: true,
  });

  const { fields: portfolios } = useFieldArray<FormValues>({
    name: 'portfolios',
    control,
    shouldUnregister: true,
  });

  const onSubmit = (data: FormValues) => {
    console.log(data);
    // console.log(errors);
    console.log(formState);
  };

  const onAddRow = () => {
    clearErrors();
    const columnsNumber = getValues('portfolios').length;
    return append({
      ticker: null,
      shares: range(columnsNumber).map(() => ({
        share: '',
      })),
    });
  };

  const onAddColumn = () => {
    clearErrors();
    const nextValue = getValues('tickers').map((ticker) => ({
      ...ticker,
      shares: [...ticker.shares, { share: '' }],
    }));
    console.log({ old: getValues('tickers'), new: nextValue });
    // console.log(nextValue);

    const columnsNumber = getValues('portfolios').length;
    const nextPortfolios = [
      ...getValues('portfolios'),
      { name: `Portfolio ${columnsNumber + 1}`, total: 0 },
    ];

    setValue('tickers', nextValue);
    setValue('portfolios', nextPortfolios);
  };

  return (
    <Container maxWidth="xl">
      <Box display="flex" flexGrow={1} alignItems="center">
        <FormProvider {...methods}>
          <form onSubmit={handleSubmit(onSubmit)}>
            <TableContainer component={Paper}>
              <Table aria-label="portfolio-input" size="small">
                <TableHead>
                  <TableRow>
                    <TableCell />
                    {portfolios.map((portfolio, index) => (
                      <TableCell key={portfolio.id} align="center">
                        <Box
                          display="flex"
                          alignItems="center"
                          justifyContent="space-between"
                        >
                          <OutlinedInput
                            {...register(`portfolios.${index}.name` as const, {
                              onChange: (e) => {
                                setValue(
                                  `portfolios.${index}.name`,
                                  e.target.value,
                                );
                              },
                            })}
                            fullWidth
                            autoComplete="off"
                          />
                        </Box>
                      </TableCell>
                    ))}
                  </TableRow>
                </TableHead>
                <TableBody>
                  {rows.map((row, tIndex) => {
                    return (
                      <TableRow key={row.id}>
                        <TickerShares tIndex={tIndex} control={control} />
                      </TableRow>
                    );
                  })}
                  <TableRow
                    sx={{
                      [`& .${tableCellClasses.body}`]: { borderBottom: 'none' },
                    }}
                  >
                    <TableCell />
                    {portfolios.map((portfolio, index) => {
                      const { total } = watchPortfolios[index];
                      const { error } = getFieldState(
                        `portfolios.${index}.total`,
                      );

                      return (
                        <TableCell align="right" key={portfolio.id}>
                          <Box>{total ? `Total: ${total} %` : ''}</Box>
                          <Box height={15}>
                            <Typography variant="body2" color="red">
                              {error?.message}
                            </Typography>
                          </Box>
                        </TableCell>
                      );
                    })}
                  </TableRow>
                </TableBody>
              </Table>
              <Box display="flex" justifyContent="center">
                <Tooltip title="Add a ticker">
                  <IconButton
                    color="primary"
                    aria-label="add a ticker"
                    size="large"
                    onClick={onAddRow}
                  >
                    <AddCircleOutline />
                  </IconButton>
                </Tooltip>
              </Box>
            </TableContainer>
            <Button type="submit">Submit</Button>
          </form>
        </FormProvider>
        <Box>
          <Tooltip title="Add a portfolio">
            <IconButton
              color="primary"
              aria-label="add a portfolio"
              size="large"
              onClick={onAddColumn}
            >
              <AddCircleOutline />
            </IconButton>
          </Tooltip>
        </Box>
      </Box>
    </Container>
  );
};

import {
  Control,
  Controller,
  useFieldArray,
  useFormContext,
} from 'react-hook-form';
import React from 'react';
import {
  Box,
  InputAdornment,
  OutlinedInput,
  TableCell,
  Typography,
} from '@mui/material';
import { isEmpty } from 'lodash';
import { FormValues, TickerRow } from './Backtester';
import { TypeWithSearch } from './TypeWithSearch';

interface Props {
  tIndex: number;
  control: Control<FormValues>;
}

export const TickerShares: React.FC<Props> = ({ tIndex, control }) => {
  const { fields: shares } = useFieldArray({
    name: `tickers[${tIndex}].shares` as 'tickers.0.shares',
    control,
    shouldUnregister: true,
  });

  const {
    setValue,
    getValues,
    watch,
    register,
    getFieldState,
    setError,
    clearErrors,
    formState: { errors },
  } = useFormContext();

  const watchFieldArray = watch(`tickers[${tIndex}].shares`);

  const controlledFields = shares.map((share, index) => {
    return {
      ...share,
      ...watchFieldArray[index],
    };
  });

  const handleChangeInput =
    (index: number) => (e: React.ChangeEvent<HTMLInputElement>) => {
      const { value } = e.target;
      setValue(`tickers.${tIndex}.shares.${index}.share`, value);
      const totalForThePortfolio = getValues('tickers')
        .map((ticker: TickerRow) => {
          const rawValue = parseFloat(ticker.shares[index].share);
          return Number.isNaN(rawValue) ? 0 : rawValue;
        })
        .reduce((a: number, b: number) => a + b, 0);

      setValue(`portfolios.${index}.total`, totalForThePortfolio);
    };

  // eslint-disable-next-line @typescript-eslint/no-unused-vars,@typescript-eslint/ban-ts-comment
  // @ts-ignore
  const handleValidate = (value: string) => {
    if (value === '') return true;
    if (!isEmpty(value.match(/[^0-9.]/gi))) {
      return 'Only numeric digits allowed';
    }
    const parsedValue = parseFloat(value);
    if (parsedValue < 0 || parsedValue > 100) {
      return 'Must be between 0 and 100';
    }
    controlledFields.forEach((_, index) => {
      const val = getValues(`portfolios.${index}.total`) || 0;
      console.log(val);
      if (val !== 0 && val !== 100) {
        console.log('setting...');
        setError(`portfolios.${index}.total`, {
          message: 'Must be 100 in total',
        });
      }
    });
    return true;
  };

  return (
    <>
      <TableCell align="center">
        <Controller
          control={control}
          name={`tickers.${tIndex}.ticker`}
          render={({ field: { onChange, value } }) => {
            return <TypeWithSearch value={value} onReportTicker={onChange} />;
          }}
        />
      </TableCell>
      {controlledFields.map((field, pIndex) => {
        const fieldName = `tickers.${tIndex}.shares.${pIndex}.share`;
        const { error } = getFieldState(fieldName);
        return (
          <TableCell key={field.id} align="center">
            <OutlinedInput
              sx={{
                marginTop: '15px',
              }}
              autoComplete="off"
              error={!!error}
              inputProps={{ inputMode: 'numeric', type: 'tel' }}
              endAdornment={<InputAdornment position="end">%</InputAdornment>}
              onClick={() => {
                clearErrors();
                // clearErrors(fieldName);
                // clearErrors(`portfolios.${pIndex}.total`);
                console.log('next errors');
                console.log(errors);
              }}
              {...register(fieldName, {
                validate: handleValidate,
                onChange: handleChangeInput(pIndex),
              })}
            />
            <Box height={15}>
              <Typography variant="body2" color="red">
                {error?.message}
              </Typography>
            </Box>
          </TableCell>
        );
      })}
    </>
  );
};

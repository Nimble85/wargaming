from functools import cache
from typing import Literal

from pydantic_settings import BaseSettings


class Settings(BaseSettings):
    APP_NAME: str = 'hackathon-backend'
    ENVIRONMENT: Literal['dev', 'prod'] = 'dev'

    class Config:
        case_sensitive = True


@cache
def get_settings() -> Settings:
    return Settings()


settings = get_settings()

from fastapi.encoders import jsonable_encoder
from fastapi.exceptions import RequestValidationError
from fastapi.responses import JSONResponse
from starlette import status
from starlette.requests import Request

from src.api.schemas import ErrorResponse, ErrorResponseMulti
from src.domain.exceptions import DomainError


def python_base_error_handler(_: Request, error: Exception) -> JSONResponse:
    response = ErrorResponseMulti(
        results=[ErrorResponse(message=f'Unhandled error: {error}')]
    )

    return JSONResponse(
        content=jsonable_encoder(response.model_dump()),
        status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
    )


def pydantic_validation_errors_handler(_: Request, error: RequestValidationError) -> JSONResponse:
    response = ErrorResponseMulti(
        results=[
            ErrorResponse(
                message=err['msg'],
                path=list(err['loc']),
            )
            for err in error.errors()
        ]
    )

    return JSONResponse(
        content=jsonable_encoder(response.model_dump()),
        status_code=status.HTTP_422_UNPROCESSABLE_ENTITY,
    )


def domain_error_handler(_: Request, __: DomainError) -> JSONResponse:
    response = ErrorResponseMulti(
        results=[ErrorResponse(message='Unhandled Domain error, please try later or contact administrator')]
    )

    return JSONResponse(
        content=jsonable_encoder(response.model_dump()),
        status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
    )

import axios, { AxiosInstance, AxiosRequestConfig } from 'axios';
import { ApiErrorHandler } from './ApiErrorHandler';

const instance: AxiosInstance = axios.create({
  baseURL: '',
});

instance.interceptors.response.use(
  (response) => response.data,
  async (error): Promise<ApiErrorHandler> => {
    console.log(error.response, 'error');
    throw new ApiErrorHandler({
      url: error.response?.config?.url,
      status: error.response?.status,
      message: error.response?.data?.error.message,
    });
  },
);

const get = async <R>(url: string, config?: AxiosRequestConfig): Promise<R> =>
  (await instance.get(url, config)) as unknown as R;
const post = async <D, R>(
  url: string,
  data?: D,
  config?: AxiosRequestConfig,
): Promise<R> => (await instance.post(url, data, config)) as unknown as R;

export { get, post };

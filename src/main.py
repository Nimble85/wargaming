from fastapi import FastAPI, Request
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates

from src.api.endpoints import root_router
from src.core.config import settings
from src.core.factory import create


app: FastAPI = create(
    title=settings.APP_NAME,
    version='0.6.2',
    rest_routers=[root_router],
    startup_tasks=[],
    shutdown_tasks=[],
)

app.mount('/assets', StaticFiles(directory='src/static/dist/assets', html=True), name='static')
templates = Jinja2Templates(directory='src/static/dist')


@app.get('/')
async def serve_spa(request: Request):
    return templates.TemplateResponse('index.html', {'request': request})

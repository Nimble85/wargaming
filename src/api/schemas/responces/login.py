from pydantic import BaseModel


class LoginResponseSchema(BaseModel):
    user_id: str
    is_admin: bool

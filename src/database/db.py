import psycopg
from pydantic_settings import BaseSettings, SettingsConfigDict


class DBConfig(BaseSettings):
    dbname: str
    user: str
    password: str
    host: str
    port: int

    model_config = SettingsConfigDict(env_file='.env')


import psycopg

class Database:
    def __init__(self):
        self.config = DBConfig().model_dump()
        self.connection = None

    def __enter__(self):
        self.connect()
        return self

    def __exit__(self, exc_type, exc_value, traceback):
        self.disconnect()

    def connect(self):
        try:
            self.connection = psycopg.connect(
                **self.config
            )
            print("Connected to database successfully.")
        except psycopg.Error as e:
            print("Unable to connect to the database:", e)

    def disconnect(self):
        if self.connection:
            self.connection.close()
            print("Disconnected from database.")

    def execute_query(self, query, params=None):
        with self.connection.cursor() as cursor:
            try:
                cursor.execute(query, params)
                self.connection.commit()
                print("Query executed successfully.")
                return cursor.fetchall()
            except psycopg.Error as e:
                print("Error executing query:", e)
                return None

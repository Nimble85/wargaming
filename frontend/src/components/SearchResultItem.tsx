import { Grid, Typography } from '@mui/material';
import * as React from 'react';
import { SymbolMatch } from '../types';

interface Props {
  option: SymbolMatch;
}

export const SearchResultItem: React.FC<Props> = ({ option }) => {
  return (
    <Grid container alignItems="center">
      <Grid
        item
        sx={{
          width: 'calc(100% - 44px)',
          wordWrap: 'break-word',
        }}
      >
        <Typography variant="subtitle1" fontWeight={700} color="text.secondary">
          {option.symbol}
        </Typography>
        <Typography variant="body1" color="text.secondary">
          {option.name}
        </Typography>
        <Typography variant="caption" color="text.secondary">
          {option.type}
        </Typography>
      </Grid>
    </Grid>
  );
};

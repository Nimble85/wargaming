import * as React from 'react';
import { Autocomplete, TextField } from '@mui/material';
import { debounce } from '@mui/material/utils';
import { useEffect, useMemo, useState } from 'react';
import { isNil } from 'lodash';
import { get } from '../services/HTTP';
import { SymbolMatch, TickersSeekResponse } from '../types';
import { SearchResultItem } from './SearchResultItem';
import { capitalizeFirstLetterInSentence } from '../utils';

interface Props {
  onReportTicker: (ticker: SymbolMatch) => void;
  value: SymbolMatch | null;
}

export const TypeWithSearch: React.FC<Props> = ({
  onReportTicker,
  value: inValue,
}) => {
  const [value, setValue] = useState<SymbolMatch | null>(inValue);
  const [inputValue, setInputValue] = useState('');
  const [options, setOptions] = useState<readonly SymbolMatch[]>([]);
  const [isLoading, setIsLoading] = useState(false);
  const fetch = useMemo(
    () =>
      debounce(async (request: { input: string }) => {
        setIsLoading(true);
        const res: TickersSeekResponse = await get(
          `https://pink-successful-chameleon.cyclic.app/api/finance/tickers/seek?query=${request.input}`,
        );
        setIsLoading(false);

        const matches = res.bestMatches.map((match) => ({
          ...match,
          name: capitalizeFirstLetterInSentence(match.name),
        }));
        setOptions(matches);
      }, 400),
    [],
  );

  useEffect(() => {
    if (inputValue === '') {
      setOptions([]);
    } else {
      fetch({ input: inputValue });
    }
  }, [value, inputValue, fetch]);

  return (
    <Autocomplete
      id="search-ticker"
      sx={{
        width: '200px',
      }}
      getOptionLabel={(option) => option.symbol}
      filterOptions={(x) => x}
      options={options}
      autoComplete
      includeInputInList
      filterSelectedOptions
      loading={isLoading}
      value={value}
      noOptionsText="No tickers found"
      onChange={(_, newValue: SymbolMatch | null) => {
        setValue(newValue);

        if (!isNil(newValue)) {
          onReportTicker(newValue);
        }
      }}
      onInputChange={(_, newInputValue) => {
        setInputValue(newInputValue);
      }}
      renderInput={(params) => (
        <TextField
          {...params}
          placeholder="Start typing"
          label="Search a ticker"
          fullWidth
        />
      )}
      isOptionEqualToValue={(option, val) => option.symbol === val.symbol}
      renderOption={(props, option) => {
        return (
          <li {...props} key={option.symbol + option.name}>
            <SearchResultItem option={option} />
          </li>
        );
      }}
    />
  );
};

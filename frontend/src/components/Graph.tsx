import React, { useRef } from 'react';
import Highcharts from 'highcharts';
import HighchartsReact from 'highcharts-react-official';

export const Graph: React.FC = () => {
  const chartComponentRef = useRef<HighchartsReact.RefObject>(null);

  const options: Highcharts.Options = {
    title: {
      text: 'My chart',
    },
    accessibility: {
      enabled: false,
    },
    credits: {
      enabled: false,
    },
    series: [
      {
        type: 'line',
        data: [1, 2, 3, 5, 2, 5, 7, 8, 4],
      },
      {
        type: 'line',
        data: [3, 1, 2, 3, 3, 2, 6, 4, 5],
        dashStyle: 'LongDash',
        color: 'red',
      },
      {
        type: 'line',
        data: [3, 1, 2, 6, 2, 1, 7, 4, 8],
        lineWidth: 5,
      },
    ],
  };
  return (
    <HighchartsReact
      highcharts={Highcharts}
      options={options}
      ref={chartComponentRef}
    />
  );
};

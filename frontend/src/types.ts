enum TickerTypes {
  EQUITY = 'Equity',
}

export interface SymbolMatch {
  symbol: string;
  name: string;
  type: TickerTypes;
  region: string;
  marketOpen: string;
  marketClose: string;
  timezone: string;
  currency: string;
  matchScore: number;
}

export interface TickersSeekResponse {
  bestMatches: SymbolMatch[];
}

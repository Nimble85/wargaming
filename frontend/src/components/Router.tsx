import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { Header } from './Header';
import { Backtester } from './Backtester';
import { About } from './About';
import { Home } from './Home.tsx';
import { Graph } from './Graph.tsx';

export const Router = () => {
  console.log('hi');

  return (
    <BrowserRouter>
      <Header />
      <Routes>
        <Route element={<Home />} path="/" />
        <Route element={<Backtester />} path="/backtester" />
        <Route element={<About />} path="/about" />
        <Route element={<Graph />} path="/graph" />
      </Routes>
    </BrowserRouter>
  );
};

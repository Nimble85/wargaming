from pydantic import BaseModel, conlist, Field


class ErrorResponse(BaseModel):
    message: str = Field(description='This field represent the message')
    path: list = Field(description='The path to the field that raised the error', default_factory=list)


class ErrorResponseMulti(BaseModel):
    results: conlist(ErrorResponse, min_length=1)

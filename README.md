# Hackathon Wargaming Backend

...

# Local run

```shell
uvicorn src.main:app --reload --host 0.0.0.0 --port 8000
```

# Run in Docker

```shell
docker build . -t hackathon-wargaming-web-backend
```

```shell
docker run --rm -p 8000:8000 hackathon-wargaming-web-backend
```
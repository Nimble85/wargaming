# Build npm dist
FROM node:20-alpine as frontend_build

WORKDIR /app/frontend

COPY frontend .

RUN npm install && npm run build

# Build fastAPI
FROM python:3.11-slim as backend_build

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

WORKDIR /app

# Installing code dependencies
COPY requirements.txt ./requirements.txt
RUN pip3 install --upgrade pip && \
    pip3 install --upgrade --no-cache-dir -r requirements.txt

# Copying sorce code
COPY src ./src
COPY --from=frontend_build /app/frontend/dist ./src/static/dist

ENTRYPOINT ["uvicorn", "src.main:app", "--reload", "--host", "0.0.0.0", "--port", "8000"]

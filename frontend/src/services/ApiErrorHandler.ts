// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-nocheck

type ErrorDetails = {
  url: string;
  status: number;
  message?: string;
};

export class ApiErrorHandler extends Error {
  private status: number;

  private date: Date;

  private url: string;

  constructor(details: ErrorDetails, ...params) {
    super(...params);

    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, ApiErrorHandler);
    }

    this.name = 'ApiError';
    this.status = details.status;
    this.url = details.url;
    this.message = details.message;
    this.date = new Date();
  }
}
